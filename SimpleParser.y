%{
	#include <stdio.h>
	#include <math.h>
	int main(void);
	void yyerror(char*);
%}

%start S
%token atom variable numeral string plus minus times dividedby
%token endofstatement nobacktrack startoflist endoflist smaller 
%token bigger equalbigger equalsmaller unifieable identic equal
%token notunifieable notidentical notequal set startbraces endbraces
%token ruleseperation pipee

%left plus minus
%left times dividedby

%%

S : E S
	| E;
	
E : atom
	| variable
	| numeral
	| string
	| plus
	| minus
	| times
	| dividedby
	| endofstatement
	| nobacktrack
	| startoflist
	| endoflist
	| smaller
	| bigger
	| equalbigger
	| equalsmaller
	| unifieable
	| identic
	| equal
	| notunifieable
	| notidentical
	| notequal
	| set
	| startbraces
	| endbraces
	| ruleseperation
	| pipee;
    
%%

int main(void) {
	yyparse();
	return 0;
}

void yyerror(char *err) {
	printf("%s",err);
}
