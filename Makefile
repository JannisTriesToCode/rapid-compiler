prolog_compiler: prolog_compiler.tab.c prolog_compiler.yy.c symbol_table.c graph.c
	gcc -Wall -g -o $@ $^

%.tab.c: %.y
	bison -v -d $<

%.yy.c: %.l
	flex -o $@ $<

