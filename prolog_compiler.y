%{
	#define YYERROR_VERBOSE 1
	#define _GNU_SOURCE 1

	#include "prolog_compiler.tab.h"
	#include "symbol_table.h"
	#include "graph.h"
	
	int yylex();
	void yyerror(const char *);
%}

%union {
	char *string;
	double number;
}

%start Start

%token<string> ATOM VARIABLE STRING NUMERAL
%token PLUS MINUS TIMES DIVIDEDBY ENDOFSTATEMENT NOBACKTRACK STARTOFLIST ENDOFLIST SMALLER  BIGGER EQUALBIGGER EQUALSMALLER UNIFIEABLE IDENTIC EQUAL NOTUNIFIEABLE NOTIDENTICAL NOTEQUAL SET STARTBRACES ENDBRACES RULESEPERATION PIPEE AND OR COMMA

%type<string> term literal arithmetics comparsion literalArg literalArgs list listArgs unificationArg identical unifieable
%left PLUS MINUS
%left TIMES DIVIDEDBY

%%

Start: fact Start
	| rule Start
	| fact
	| rule;

rule: literal {createTerm($1);} RULESEPERATION terms ENDOFSTATEMENT {createClause();};

fact: literal {createTerm($1);} ENDOFSTATEMENT {createClause();};

terms: term {createTerm($1);} COMMA terms 
	| term {createTerm($1);};

term: literal 
	| comparsion
	| arithmetics
	| VARIABLE SET arithmetics {asprintf(&$$, "is(%s,%s)", $1, $3);}
	| unifieable
	| identical
	| NOBACKTRACK {$$ = strdup("!");};

unifieable: unificationArg UNIFIEABLE unificationArg {asprintf(&$$, "=(%s,%s)", $1, $3);}
	| unificationArg NOTUNIFIEABLE unificationArg {asprintf(&$$, "\\=(%s,%s)", $1, $3);};
	
identical: unificationArg IDENTIC unificationArg {asprintf(&$$, "==(%s,%s)", $1, $3);}
	| unificationArg NOTIDENTICAL unificationArg {asprintf(&$$, "\\==(%s,%s)", $1, $3);};
	
unificationArg: VARIABLE {createVariable($1);}
	| NUMERAL
	| STRING
	| literal
	| list
	| NOBACKTRACK {$$ = strdup("!");};

list: STARTOFLIST ENDOFLIST {$$ = strdup("[]");}
	| STARTOFLIST listArgs ENDOFLIST {asprintf(&$$, "[%s]", $2);};
	
listArgs: literalArgs
	| literalArgs PIPEE literalArg {asprintf(&$$, "%s|%s", $1, $3);};
	
literal: ATOM STARTBRACES literalArgs ENDBRACES {asprintf(&$$, "%s(%s)", $1, $3);}
	| ATOM STARTBRACES ENDBRACES {asprintf(&$$, "%s()", $1);}
	| ATOM;
	
literalArgs: literalArg COMMA literalArgs {asprintf(&$$, "%s,%s", $1, $3);}
	| literalArg;
	
literalArg: STRING
	| list
	| arithmetics
	| literal;
	
comparsion: arithmetics EQUAL arithmetics {asprintf(&$$, "=:=(%s,%s)", $1, $3);}
	| arithmetics NOTEQUAL arithmetics {asprintf(&$$, "=\\=(%s,%s)", $1, $3);}
	| arithmetics SMALLER arithmetics {asprintf(&$$, "<(%s,%s)", $1, $3);}
	| arithmetics EQUALSMALLER arithmetics {asprintf(&$$, "=<(%s,%s)", $1, $3);}
	| arithmetics BIGGER arithmetics {asprintf(&$$, ">(%s,%s)", $1, $3);}
	| arithmetics EQUALBIGGER arithmetics {asprintf(&$$, ">=(%s,%s)", $1, $3);};
	
arithmetics: NUMERAL
	| VARIABLE {createVariable($1);}
	| PLUS arithmetics {asprintf(&$$, "%s", $2);}
	| MINUS arithmetics {asprintf(&$$, "-(%s)", $2);}
	| arithmetics PLUS arithmetics {asprintf(&$$, "+(%s,%s)", $1, $3);}
	| arithmetics MINUS arithmetics {asprintf(&$$, "-(%s,%s)", $1, $3);}
	| arithmetics TIMES arithmetics {asprintf(&$$, "*(%s,%s)", $1, $3);}
	| arithmetics DIVIDEDBY arithmetics {asprintf(&$$, "/(%s,%s)", $1, $3);}
	| STARTBRACES arithmetics ENDBRACES {asprintf(&$$, "%s", $2);};

%%

int main() {
	
	yyparse();

	printSortedSymbolTable();

	makeGraph();
}

void yyerror(const char* e) {
	printf("Error: %s\n", e);
}