#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * linked list of rules/facts
 * contains a linked list of all terms in the clause
 */
struct clause {
    struct term* terms;
    struct clause* next;
};

/*
 * linked list of terms
 * contains the whole term as string
 * contains a linked list of all variable for the term
 */
struct term {
    char* value;
    struct variable* variables;
    struct term* next;
    struct node* result;
};

/*
 * linked list of variables
 * contains the variable as string
 */
struct variable {
    char* value;
    bool isNew;
    struct variable* next;
};

/*
 * linked list putting the clauses into categories
 * regarding the predicate and count of variables
 */
struct distinctClause {
    char* predicate;
    int variableCount;
    int clauseCount;
    struct clause* clausesFirst;
    struct clause* clausesLast;
    struct distinctClause* next;
};

void createVariable(char* string);
void createTerm(char* string);
void createClause();
void seperateClause(struct clause* clause);
int getVariableCount(struct variable* variables);
void printSortedSymbolTable();
