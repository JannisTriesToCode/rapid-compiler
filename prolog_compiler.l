%{
	#include "prolog_compiler.tab.h"
%}

%option noyywrap
%option nounput
%option noinput


lc_letter 		[a-z]
uc_letter 		[A-Z_]
digit 			[0-9]
alphanum 		{lc_letter}|{uc_letter}|{digit}

atom 			{lc_letter}{alphanum}*
variable 		{uc_letter}{alphanum}*
numeral 		{digit}+(.{digit}+)?
string 			".+"

%%
is 				return SET;

{atom} 			{yylval.string = strdup(yytext); return ATOM;}
{variable}		{yylval.string = strdup(yytext); return VARIABLE;}
{numeral} 		{yylval.string = strdup(yytext); return NUMERAL;}
{string} 		{yylval.string = strdup(yytext); return STRING;}

\+ 				return PLUS;
\- 				return MINUS;    
\* 				return TIMES;
\/ 				return DIVIDEDBY;
\. 				return ENDOFSTATEMENT;
\! 				return NOBACKTRACK;
\[ 				return STARTOFLIST;
\] 				return ENDOFLIST;
\< 				return SMALLER;
\> 				return BIGGER;
\>\= 			return EQUALBIGGER;
\=\< 			return EQUALSMALLER;
\= 				return UNIFIEABLE;
\=\= 			return IDENTIC;
\=\:\= 			return EQUAL;
\\\= 			return NOTUNIFIEABLE; 
\\\=\= 			return NOTIDENTICAL;
\=\\\= 			return NOTEQUAL;
\(		 		return STARTBRACES;
\) 				return ENDBRACES;
\:\- 			return RULESEPERATION;
\| 				return PIPEE;
\, 				return COMMA;

\%.*\n 
[ \n]

%%
