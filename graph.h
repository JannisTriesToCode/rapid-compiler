#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct node {
    char type;
    char* subgoal;
    int index;
    int targetCount;
    int nextTarget;
    struct target* targets;
    struct node* next;
};

struct target {
    struct node* node;
    int port;
};

struct nonDependentExit {
	struct node *node;
	struct nonDependentExit *next;
};


void makeGraph();