#include "graph.h"
#include "symbol_table.h"

struct node *firstNode;
struct node *lastNode;
int nodeIndex = 1;

extern struct distinctClause *distinctClauseFirst;
struct nonDependentExit *firstNonDependentExit = NULL;
struct nonDependentExit *lastNonDependentExit = NULL;

// type can only be 'E', 'C','U','A' or 'R'
struct node *appendNode(char type, int targetCount) {
    struct node *node = calloc(1, sizeof(struct node));

    node->type = type;
    node->index = nodeIndex;
    node->targetCount = targetCount;
    node->targets = calloc(targetCount, sizeof(struct target));

    if (firstNode == 0) {
        firstNode = node;
    } else {
        lastNode->next = node;
    }

    lastNode = node;
    nodeIndex++;

    return node;
}

// check if a variable is in a term
bool varInTerm(struct term *term, struct variable *variable) {
    struct variable *varInTerm = term->variables;
    while (varInTerm) {
        if (!strcmp(variable->value, varInTerm->value)) {
            return true;
        }
        varInTerm = varInTerm->next;
    }

    return false;
}

// test the unconditioned dependency between the given term and leftover terms in its clause
bool testUnconditionedDependency(struct term *term, struct term *leftoverTerm) {
    struct variable *leftoverVariable = leftoverTerm->variables;
    while (leftoverVariable) {
        if (!varInTerm(term, leftoverVariable) || !leftoverVariable->isNew) {
            leftoverVariable = leftoverVariable->next;
            continue;
        }
        return true;
    }

    return false;
}

// connects all non-dependent exits to the given node
void connectNonDependentExits(struct node *targetNode) {
    struct nonDependentExit *nonDependentExit = firstNonDependentExit;

    while (nonDependentExit) {
        nonDependentExit->node->targets[1].node = targetNode;
        nonDependentExit->node->targets[1].port = 1;
        nonDependentExit = nonDependentExit->next;
    }

    firstNonDependentExit = NULL;
    lastNonDependentExit = NULL;
}

void makeGraph() {
    FILE *file = fopen("graph.txt", "w");

    struct distinctClause *distinctClausePointer = distinctClauseFirst;
    struct clause *clausePointer;

    while (distinctClausePointer) {
        clausePointer = distinctClausePointer->clausesFirst;
        while (clausePointer) {
            struct term *headTerm = clausePointer->terms;
            struct term *bodyTerm = headTerm->next;

            int termCount = 0;
            struct term *term = bodyTerm;
            while (term) {
                term = term->next;
                termCount++;
            }

            // add entry node
            struct node *entry = appendNode('E', 2);
            entry->subgoal = headTerm->value;
            entry->nextTarget = 1;
            struct node *nodePointer = entry;
            struct node *firstCopyPointer = entry;

            // add copy node if needed
            if (termCount > 1) {
                struct node *copy = appendNode('C', termCount);
                firstCopyPointer->targets[1].node = copy;
                firstCopyPointer->targets[1].port = 1;
                firstCopyPointer = copy;
            }

            // add all other nodes
            term = bodyTerm;
            int i = 1, bodyTermCount = termCount - 1;
            while (term) {
                // add update node
                struct node *update = appendNode('U', 1);
                update->subgoal = term->value;
                firstCopyPointer->targets[firstCopyPointer->nextTarget].node = update;
                firstCopyPointer->targets[firstCopyPointer->nextTarget].port = 1;
                firstCopyPointer->nextTarget++;

                struct term *leftoverTerm = bodyTerm;
                while (leftoverTerm != term) {
                    if (testUnconditionedDependency(term, leftoverTerm)) {
                        // add update node
                        struct node *dependencyUpdate = appendNode('U', 1);
                        update->targets[0].node = dependencyUpdate;
                        update->targets[0].port = 2;
                        struct node *dependencyResult = leftoverTerm->result;
                        dependencyResult->targets[dependencyResult->nextTarget].node = dependencyUpdate;
                        dependencyResult->targets[dependencyResult->nextTarget].port = 1;
                        dependencyResult->nextTarget++;

                        update = dependencyUpdate;
                        //connectNonDependentExits(dependencyUpdate);
                    }

                    leftoverTerm = leftoverTerm->next;
                }

                // add apply node
                struct node *apply = appendNode('A', 1);
                update->targets[0].node = apply;
                update->targets[0].port = 1;
                struct node *result = apply;

                // set if to 'bodyTermCount > 0' to skip redundant c nodes
                if (true) {
                    struct node *copy = appendNode('C', bodyTermCount + 1);
                    apply->targets[0].node = copy;
                    apply->targets[0].port = 1;
                    result = copy;
                }

                term->result = result;

                // add update node
                update = appendNode('U', 1);
                result->targets[result->nextTarget].node = update;
                result->targets[result->nextTarget].port = 1;
                result->nextTarget++;
                nodePointer->targets[0].node = update;
                nodePointer->targets[0].port = 2;
                nodePointer = update;

                term = term->next;
                bodyTermCount--;
                i++;
            }

            // add return node -> finishes the graph for the clause
            struct node *return_ = appendNode('R', 0);
            nodePointer->targets[0].node = return_;
            nodePointer->targets[0].port = 1;

            // write all nodes of the current clause into a file
            struct node *node = firstNode;
            while (node) {
                fprintf(file, "%d\t%c", node->index, node->type);

                i = 0;
                while ((i < node->targetCount) || (i < 2)) {
                    if (i < node->targetCount) {
                        struct target *target = node->targets + i;
                        if (target->node) {
                            fprintf(file, "\t(%d,%d)", target->node->index, target->port);
                        } else {
                            fprintf(file, "\t-");
                        }
                    } else {
                        fprintf(file, "\t-");
                    }

                    i++;
                }

                if (node->subgoal) {
                    fprintf(file, "\t%s", node->subgoal);
                } else if (node->type != 'C') {
                    fprintf(file, "\t-");
                }

                fprintf(file, "\n");
                node = node->next;
                nodeIndex++;
            }

            // reset nodes
            firstNode = 0;
            lastNode = 0;

            clausePointer = clausePointer->next;
        }
        distinctClausePointer = distinctClausePointer->next;
    }
}