#include "symbol_table.h"

struct term* termsFirst;
struct term* termsLast;
struct variable* variablesFirst;
struct variable* variablesLast;
struct distinctClause* distinctClauseFirst;
struct distinctClause* distinctClauseLast;

/*
 * createVariable(char* string)
 * creates a struct variable, fills it with the variable as string
 * appends struct to list of variables
 */
void createVariable(char* string) {
    struct variable* variable;
    variable = calloc(1, sizeof(struct variable));

    variable->value = string;
    variable->isNew = true;

    struct term* termPointer = termsFirst;
    while (termPointer) {
        struct variable* variablePointer = variablesFirst;
        while (variablePointer) {
            if (variablePointer->value == variable->value) {
                variable->isNew = false;
            }
            variablePointer = variablePointer->next;
        }
        termPointer = termPointer->next;
    }

    if (variablesFirst == 0) {
        variablesFirst = variable;
    } else {
        variablesLast->next = variable;
    }

    variablesLast = variable;
}

/*
 * createTerm(char* string)
 * creates a struct term, fills it with the whole term as string and with
 * pointer to corresponding linked list of variables sets pointer of global
 * linked list of variables to zero, so it can be reused for next term appends
 * struct to list of terms
 */
void createTerm(char* string) {
    struct term* term;
    term = calloc(1, sizeof(struct term));

    term->value = string;
    term->variables = variablesFirst;

    variablesFirst = 0;

    if (termsFirst == 0) {
        termsFirst = term;
    } else {
        termsLast->next = term;
    }

    termsLast = term;
}

/*
 * createClause()
 * creates struct clause, fills it with pointer to corresponding linked list of
 * terms sets pointer of global linked list of terms to zero, so it can be
 * reused for next clause appends struct to list of clauses
 */
void createClause() {
    struct clause* clause;
    clause = calloc(1, sizeof(struct clause));

    clause->terms = termsFirst;

    termsFirst = 0;

    seperateClause(clause);
}

/*
 * seperateClause(struct clause * clause)
 * gets clause and sorts it into respective class of clauses
 * classes are seperated by different literals and variable counts
 */
void seperateClause(struct clause* clause) {
    char* temp = calloc(1, sizeof(char) * (strlen(clause->terms->value) + 1));
    strcpy(temp, clause->terms->value);
    char* predicate = strtok(temp, "(");
    int variableCount = getVariableCount(clause->terms->variables);

    if (distinctClauseFirst == 0) {
        struct distinctClause* distinctClause;
        distinctClause = calloc(1, sizeof(struct distinctClause));

        distinctClause->clausesFirst = clause;
        distinctClause->clausesLast = clause;
        distinctClause->predicate = predicate;
        distinctClause->variableCount = variableCount;
        distinctClause->clauseCount = 1;

        distinctClauseFirst = distinctClause;
        distinctClauseLast = distinctClause;
    } else {
        struct distinctClause* distinctClausePointer = distinctClauseFirst;
        while (distinctClausePointer) {
            if (!strcmp(distinctClausePointer->predicate, predicate) && (distinctClausePointer->variableCount == variableCount)) {
                distinctClausePointer->clausesLast->next = clause;
                distinctClausePointer->clausesLast = clause;
                distinctClausePointer->clauseCount += 1;

                return;
            } else if (distinctClausePointer->next == 0) {
                struct distinctClause* distinctClause;
                distinctClause = calloc(1, sizeof(struct distinctClause));

                distinctClause->clausesFirst = clause;
                distinctClause->clausesLast = clause;
                distinctClause->predicate = predicate;
                distinctClause->variableCount = variableCount;
                distinctClause->clauseCount = 1;

                distinctClauseLast->next = distinctClause;
                distinctClauseLast = distinctClause;

                return;
            }
            distinctClausePointer = distinctClausePointer->next;
        }
    }
}

int getVariableCount(struct variable* variables) {
    int counter = 0;
    while (variables) {
        counter++;
        variables = variables->next;
    }
    return counter;
}

void printSortedSymbolTable() {
    int i = 1;
    struct distinctClause* distinctClausePointer = distinctClauseFirst;
    struct clause* clausePointer;
    struct term* termPointer;
    struct variable* variablePointer;

    // print all clauses
    while (distinctClausePointer) {
        printf("\n\n%s(", distinctClausePointer->predicate);
        if (distinctClausePointer->variableCount > 0)
            printf("_");
        for (int j = 1; j < distinctClausePointer->variableCount; j++) {
            printf(",_");
        }
        printf(")");
        clausePointer = distinctClausePointer->clausesFirst;
        while (clausePointer) {
            printf("\n  %d. clause:", i);
            termPointer = clausePointer->terms;
            while (termPointer) {
                printf("\n\tterm:\t%-20svariables:\t", termPointer->value);
                variablePointer = termPointer->variables;
                while (variablePointer) {
                    printf("%s", variablePointer->value);
                    variablePointer = variablePointer->next;
                }
                termPointer = termPointer->next;
            }
            clausePointer = clausePointer->next;
            i++;
        }
        distinctClausePointer = distinctClausePointer->next;
        i = 1;
    }
    printf("\n");
}